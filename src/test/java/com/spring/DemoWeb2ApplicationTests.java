//package com.spring;
//
//import static org.junit.Assert.assertTrue;
//
//import org.junit.jupiter.api.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import com.spring.model.Usuario;
//import com.spring.repo.IUsuarioRepo;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//class DemoWeb2ApplicationTests {
//
//	@Autowired
//	private IUsuarioRepo repo;
//
//	@Autowired
//	private BCryptPasswordEncoder encoder;
//
//	@Test
//	void crearUsuario() {
//
//		Usuario usuario = new Usuario();
//
//		usuario.setId(3);
//		usuario.setNombre("pilar");
//		usuario.setClave(encoder.encode("123"));
//
//		Usuario retorno = repo.save(usuario);
//
//		assertTrue(retorno.getClave().equalsIgnoreCase(usuario.getClave()));
//
//
//	}
//
//}
